package uz.azn.x_0game

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.view.size
import uz.azn.x_0game.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    var activePlayer = (0 until 2).random()
    var isGameActive = true

    var count1 = 0
    var count2 = 0

    // oynalmagan katak
    var gameState = arrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2)

    var isHavveTwo = false
    var nameTwo2 = ""
    var nameOne2 = ""
    // yutush kataklari
    var winningPosition = arrayOf(
        arrayOf(0, 1, 2),
        arrayOf(3, 4, 5),
        arrayOf(6, 7, 8),
        arrayOf(0, 3, 6),
        arrayOf(1, 4, 7),
        arrayOf(2, 5, 8),
        arrayOf(0, 4, 8),
        arrayOf(2, 4, 6)
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val nameOne = intent.getStringExtra("nameOne")!!
        nameOne2 = nameOne
        val isHave = intent.getBooleanExtra("isHave",false)

        if (isHave){
            isHavveTwo = isHave
            binding.nameTwo.visibility = View.INVISIBLE
            binding.countTwo.visibility = View.INVISIBLE
            binding.nameOne.text = "$nameOne:"
        }
        else{
            val nameTwo  = intent.getStringExtra("nameTwo")!!

            nameTwo2 =nameTwo
            binding.nameOne.text = "$nameOne:"
            binding.nameTwo.text = "$nameTwo:"

        }
//
//        if (nameTwo!!.isEmpty()){
//            binding.nameTwo.visibility = View.INVISIBLE
//            binding.nameOne.text = nameOne
//        }
//        else{
//
//
//        }

        playAgain(binding.playAgainLayout)
    }

    fun dropIn(view: View) {
        if (isGameActive) {
            val counter = view as ImageView
            val tapped = counter.tag.toString().toInt()

            if (gameState[tapped] == 2) {
                gameState[tapped] = activePlayer

                counter.rotation = 0f
                counter.translationY = -1000f
                if (activePlayer == 0) {
                    binding.nameTwo.setTextColor(Color.WHITE)
                    counter.setImageResource(R.drawable.ic_x)
                    binding.nameOne.setTextColor(Color.RED)
                    activePlayer = 1
                } else {
                    binding.nameOne.setTextColor(Color.WHITE)
                    counter.setImageResource(R.drawable.ic_o)
                    binding.nameTwo.setTextColor(Color.BLUE)
                    activePlayer = 0
                }
                counter.animate().translationYBy(1000f).rotation(360f).duration = 500
                for (winningPosition in winningPosition) {
                    if (gameState[winningPosition[0]] == gameState[winningPosition[1]]
                        && gameState[winningPosition[1]] == gameState[winningPosition[2]]
                        && gameState[winningPosition[0]] != 2

                    ) {
                        isGameActive = false
                        var winerr = nameOne2
                        if (gameState[winningPosition[0]] == 1) winerr = nameTwo2
                        binding.winnerTv.text = "$winerr has won"

                        if (isHavveTwo){
                            if (binding.winnerTv.text.contains(nameOne2)){
                                count1++
                                binding.countOne.text = count1.toString()
                            }
                            else{
                                binding.winnerTv.text = " Has Lose"

                            }
                        }
                        else{
                            if (binding.winnerTv.text.contains(nameOne2)){
                                count1++
                                binding.countOne.text = count1.toString()
                            }
                            else{
                                count2++
                                binding.countTwo.text = count2.toString()
                            }
                        }
                        binding.playAgainLayout.visibility = View.VISIBLE
                    } else {
                        var isGameOver = true

                        for (counterState in gameState) {
                            if (counterState == 2) isGameOver = false
                        }
                        if (isGameOver) {
                            binding.winnerTv.text = "Draw!"
                            binding.playAgainLayout.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }

    }

    fun playAgain(view: View) {
        isGameActive = true
        binding.playAgainLayout.visibility = View.INVISIBLE
        activePlayer = (0 until 2).random()

        for (i in 0..gameState.lastIndex) {
            gameState[i] = 2
        }
        for (i in 0 until binding.gridLayout.childCount) {
            (binding.gridLayout.getChildAt(i) as ImageView).setImageResource(0)
        }
    }
}