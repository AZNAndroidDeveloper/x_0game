package uz.azn.x_0game

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import uz.azn.x_0game.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {
    val binding by lazy { ActivitySplashBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val intent = Intent(applicationContext, MainActivity::class.java)

        binding.personOne.setOnClickListener {
            binding.linearLayoutOne.visibility = View.INVISIBLE
            binding.linearLayoutTwo.visibility = View.VISIBLE
            binding.editOne.visibility = View.VISIBLE
            binding.editTwo.visibility = View.INVISIBLE
            binding.saveButton.setOnClickListener {

                if (binding.editOne.text.isNotEmpty()){
                    intent.putExtra("nameOne", binding.editOne.text.toString())
                    intent.putExtra("isHave",true)
                    startActivity(intent)
                    finish()
                }
        }


        }
        binding.personTwo.setOnClickListener {
            binding.linearLayoutOne.visibility = View.INVISIBLE
            binding.linearLayoutTwo.visibility = View.VISIBLE
            binding.editOne.visibility = View.VISIBLE
            binding.editTwo.visibility = View.VISIBLE
            binding.saveButton.setOnClickListener {
                if (binding.editOne.text.toString().isNotEmpty() && binding.editTwo.text.toString()
                        .isNotEmpty()
                ) {
                    intent.putExtra("nameOne", binding.editOne.text.toString())
                    intent.putExtra("nameTwo", binding.editTwo.text.toString())
                    intent.putExtra("isHave", false)
                    startActivity(intent)
                    finish()
                }
            }
        }
        binding.back.setOnClickListener {
            binding.linearLayoutTwo.visibility = View.INVISIBLE
            binding.linearLayoutOne.visibility = View.VISIBLE
            binding.editTwo.text.clear()
            binding.editOne.text.clear()
        }
    }
}